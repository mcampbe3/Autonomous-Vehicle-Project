import controlP5.*;
import processing.net.*;
import meter.*;

ControlP5 cp5;
Client myClient;
String data;
Meter m;


void setup() {
  size(800, 800);
  frameRate(30);
  cp5 = new ControlP5(this);
  myClient = new Client(this,"192.168.4.1",80);
 
  cp5.addButton("Start")
    .setValue(0)
    .setPosition(100, 150)
    .setSize(150, 75); 

  cp5.addButton("Stop")
    .setValue(0)
    .setPosition(300, 150)
    .setSize(150, 75); 
    
  m = new Meter(this, 200, 500);
    m.setDisplayDigitalMeterValue(true);

}


void draw() {
  //int distance = 0;
  int enable_signal = 0;
  if (myClient.active()){
    //if (myClient.available() > 0){
      //distance = myClient.read();
      //println("Distance read in." + "\n");
    //}
    
    if (myClient.available() > 0) {
      enable_signal = myClient.read();
      println("Enable signal read in." + "\n");
    }
    
    //println("Distance: " + distance + "\n");
    println("Signal: " + enable_signal + "\n");
    
    m.updateMeter(enable_signal);
  }
}

public void Start() {
  if (myClient.active()){
    myClient.write("t");
    println("'Start' button pressed.");   
    loop();
  }
}

public void Stop() {
  if  (myClient.active()){
    myClient.write("s");
    println("'Stop' button pressed.");  
  }  
}
