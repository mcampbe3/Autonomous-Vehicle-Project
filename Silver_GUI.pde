import controlP5.*;
import processing.net.*;

ControlP5 cp5;
Client myClient;
String data;


void setup() {
  size(600,600);
  cp5 = new ControlP5(this);
  myClient = new Client(this,"192.168.4.1",80);
 
  cp5.addButton("Start")
    .setValue(0)
    .setPosition(100, 150)
    .setSize(150, 75); 

  cp5.addButton("Stop")
    .setValue(0)
    .setPosition(300, 150)
    .setSize(150, 75); 
    
 cp5.addButton("Status")
   .setValue(0)
   .setPosition(200,250)
   .setSize(150,150);
}


void draw() {
}

public void Start() {
  if (myClient.active()){
    myClient.write("t");
    println("'Start' button pressed.");  
    
    int d = 0;
    d = myClient.read();
    
    if ((d > 8) && (d < 40)) {
      println("Guide detected. Following...");
    }
 
    else {
      println("There is a problem. Buggy stopped.");
    }  
  }
}

public void Stop() {
  if  (myClient.active()){
    myClient.write("s");
    println("'Stop' button pressed.");  
  }  
}

public void Status() {
  if  (myClient.active()){
    println("Status: ");
    
    int d = 0;
    d = myClient.read();
    
    if ((d > 8) && (d < 40)) {
      println("Following...");
    }
    
    else if (d < 8) {
      println("Guide too close! Buggy stopped.");
    }
    
    else {
      println("Guid out of range. Buggy stopped.");
    }  
  }
}
