// currently the motors will drive at a rate dictated by the 'enable_signal' variable (see definition of Start_Lap())
// develop an algorithm that changes the value of this variable according to the distance measured by the ultrasonic sensor

//#include <WiFiNINA.h>

//WiFi 
/*char ssid[] = "hello2";
char pass[] = "matthewhs";
WiFiServer server(80);*/

///////////////////////////////////////////////////////////////////////////////////////////

//Arduino Pins

const int US_TRIG = 14;
const int US_ECHO = 15;

const int LEYE = 21;
const int REYE = 13;

const int en1 = 17;
const int en2 = 16;

const int IN1 = 3; 
const int IN2 = 2; 
const int IN3 = 9; 
const int IN4 = 10; 

///////////////////////////////////////////////////////////////////////////////////////////////

// Bronze Variables

//char user_input;
int d = 11;
//int flag = 0;

//////////////////////////////////////////////////////////////////////////////////////////////

// Silver Variables

//PID constants
double kp = 10;
double ki = 0;
double kd = 1;

// Variables used in calculation of PID
unsigned long currentTime, previousTime;
double elapsedTime;
double error;
double lastError;
double input, output; 
double setPoint = 5;
double cumError = 0; 
double rateError = 0;

// Enable signal constraints
int enable_min = 0;
int enable_max = 255;

//////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
  Serial.begin( 9600 );
 
  //Ultrasonic Pins
  pinMode(US_TRIG, OUTPUT);
  pinMode(US_ECHO, INPUT);

  //IR eye pins
  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );

  //Motor pins
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);

  //WIFI & IP address
  /*WiFi.beginAP (ssid, pass);
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address:");
  Serial.println(ip); 
  server.begin();*/
}

void loop() {
  Start_Lap();
}

//////////////////////////////////////////////////////////////////////////////////////////////

void Start_Lap(){

  int distance;
  long duration;
 
  digitalWrite( US_TRIG, LOW );
  delayMicroseconds(2);
  digitalWrite( US_TRIG, HIGH );
  delayMicroseconds( 10 );
  digitalWrite( US_TRIG, LOW );

  duration = pulseIn( US_ECHO, HIGH );
  distance = duration/58;

  //Serial.print("Distance detected: ");
  //Serial.print( distance );
  //Serial.println(" cm");

  int enable_signal = - computePID(distance);
  
  if (enable_signal < 0) {
    enable_signal = enable_min;
  }

  if (enable_signal > 255) {
    enable_signal = enable_max;
  }
  
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.print("  ");
  Serial.print("Enable Signal Value: ");
  Serial.print(enable_signal);
  Serial.print("\n");
  //enable_signal = 255;

  Serial.print("\n ///////////////////////////////////////////////////////////////////// \n");
  


  if (( digitalRead( LEYE ) == HIGH )&&( digitalRead( REYE ) == LOW )){
    turn_left(enable_signal);
  } 
  
  else if (( digitalRead( LEYE ) == LOW )&&( digitalRead( REYE ) == HIGH )){
    turn_right(enable_signal);
  }
   
  else if (( digitalRead( LEYE ) == LOW )&&( digitalRead( REYE ) == LOW )){
    both_forward(enable_signal);
  } 
  
  else {
    both_stop();
  }


}

/////////////////////////////////////////////////////////////////////////////////////////////

double computePID(double inp){ 
  currentTime = millis();                                 // get current time
  elapsedTime = currentTime - previousTime;               // compute time elapsed from previous computation
        
  error = setPoint - inp;                                 // determine error
  cumError += error * elapsedTime;                        // compute integral
  rateError = (error - lastError)/elapsedTime;            // compute derivative

  double out = kp*error + ki*cumError + kd*rateError;     // PID output               
 
  lastError = error;                                      // remember current error
  previousTime = currentTime;                             // remember current time
 
  return out;                                             // have function return the PID output
}

/////////////////////////////////////////////////////////////////////////////////////////////

void both_forward (int enable_signal)
{
  //left motor forward
  analogWrite(en1, enable_signal);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

  //right motor forward
  analogWrite(en2, enable_signal);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
}

void turn_right (int enable_signal) 
{
  //left wheel forward
  analogWrite(en1, enable_signal);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

  // right wheel slow
  int half_enable = enable_signal/2;
  analogWrite(en2, half_enable);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
}

void turn_left (int enable_signal)
{
  //left wheel slow
  int half_enable = enable_signal/2;
  analogWrite(en1, half_enable);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

  //right wheel forward
  analogWrite(en2, enable_signal);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
}

void both_stop()
{
  //left motor stop
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,LOW);

  //right motor stop
  digitalWrite(IN3,LOW);
  digitalWrite(IN4,LOW);
}
