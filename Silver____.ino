#include <WiFiNINA.h>

//WiFi 
char ssid[] = "hello2";
char pass[] = "matthewhs";
WiFiServer server(80);
WiFiClient client;

//Arduino Pins

const int US_TRIG = 14;
const int US_ECHO = 15;

const int LEYE = 21;
const int REYE = 13;

const int en1 = 17;
const int en2 = 16;

const int IN1 = 3; 
const int IN2 = 2; 
const int IN3 = 9; 
const int IN4 = 10; 

// Bronze Variables

int d = 11;
int flag = 0;
int distance = 0;

// Silver Variables

//PID constants
int enable_signal = 0;
double kp = 12;
double ki = 0.00001;
double kd = 2;

// Variables used in calculation of PID
unsigned long currentTime, previousTime;
double elapsedTime;
double error;
double lastError;
double input, output; 
double setPoint = 9;
double cumError = 0; 
double rateError = 0;

//////////////////////////////////////////////////////////////////////////////////////////////

void setup() {
  Serial.begin( 9600 );
 
  //Ultrasonic Pins
  pinMode(US_TRIG, OUTPUT);
  pinMode(US_ECHO, INPUT);

  //IR eye pins
  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );

  //Motor pins
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);

  //WIFI & IP address
  WiFi.beginAP (ssid, pass);
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address:");
  Serial.println(ip); 
  server.begin();
}

void loop() {
  
  if(flag == 1){
    Start_Lap();
  }

  if (flag == 2){
    both_stop();
  }

  client = server.available();

  if (client.connected()) {
    Serial.println("Client Connected");
    char c = client.read();
    Serial.println(c);

    
    // If the Start Button is Pressed
    if(c == 't'){
      flag = 1; // begin continuous repetition of Start_Lap()
    }
    
    // If the Stop Button is Pressed
    if (c == 's'){
      flag = 2; // begin continuous repetition of both_stop()
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////

void Start_Lap(){

  //int distance;
  long duration;
 
  digitalWrite( US_TRIG, LOW );
  delayMicroseconds(2);
  digitalWrite( US_TRIG, HIGH );
  delayMicroseconds( 10 );
  digitalWrite( US_TRIG, LOW );

  duration = pulseIn( US_ECHO, HIGH );
  distance = duration/58;

  enable_signal = computePID(distance); 
  
  if (enable_signal < 0) {
    both_stop();
    enable_signal = 0;
    //client.write(distance);
    client.write(enable_signal);
  }

  else if (enable_signal > 255) {
    both_stop();
    enable_signal = 0;
    //client.write(distance);
    client.write(enable_signal);
  }
  
  else {

    //client.write(distance);
    client.write(enable_signal);
    
    if (( digitalRead( LEYE ) == HIGH )&&( digitalRead( REYE ) == LOW )){
      turn_left(enable_signal);
    } 
    
    else if (( digitalRead( LEYE ) == LOW )&&( digitalRead( REYE ) == HIGH )){
      turn_right(enable_signal);
    }
     
    else if (( digitalRead( LEYE ) == LOW )&&( digitalRead( REYE ) == LOW )){
      both_forward(enable_signal);
    } 
    
    else {
      both_stop();
    }
  }

}

/////////////////////////////////////////////////////////////////////////////////////////////

double computePID(double inp){ 
  currentTime = millis();                                 // get current time
  elapsedTime = currentTime - previousTime;               // compute time elapsed from previous computation
        
  error = setPoint - inp;                                 // determine error
  cumError += error * elapsedTime;                        // compute integral
  rateError = (error - lastError)/elapsedTime;            // compute derivative

  double out = (kp*error + ki*cumError + kd*rateError)*-1;     // PID output               
 
  lastError = error;                                      // remember current error
  previousTime = currentTime;                             // remember current time
 
  return out;                                             // have function return the PID output
}

/////////////////////////////////////////////////////////////////////////////////////////////

void both_forward (int enable_signal)
{
  //left motor forward
  analogWrite(en1, enable_signal);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

  //right motor forward
  analogWrite(en2, enable_signal);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
}

void turn_right (int enable_signal) 
{
  //left wheel forward
  analogWrite(en1, enable_signal);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

  // right wheel slow
  int half_enable = enable_signal/2;
  analogWrite(en2, half_enable);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
}

void turn_left (int enable_signal)
{
  //left wheel slow
  int half_enable = enable_signal/2;
  analogWrite(en1, half_enable);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

  //right wheel forward
  analogWrite(en2, enable_signal);
  digitalWrite(IN3,HIGH);
  digitalWrite(IN4,LOW);
}

void both_stop()
{
  //left motor stop
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,LOW);

  //right motor stop
  digitalWrite(IN3,LOW);
  digitalWrite(IN4,LOW);
}
