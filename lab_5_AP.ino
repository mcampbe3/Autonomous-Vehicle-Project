#include <WiFiNINA.h>

char ssid[] = "hello";

char pass[] = "matthewhs";

WiFiServer server(80);

const int IN1 = 3;

const int IN2 = 2;

const int IN3 = 9;
const int IN4 = 10;



void setup() {

 Serial.begin(9600); 

 pinMode (IN1, OUTPUT);

 pinMode (IN2, OUTPUT);

 pinMode (IN3, OUTPUT);

 pinMode (IN4, OUTPUT);

 WiFi.beginAP (ssid, pass);

 IPAddress ip = WiFi.localIP();

 Serial.print("IP Address:");

 Serial.println(ip); 

 server.begin();

 }
 
void loop() {

 WiFiClient client = server.available();

 if (client.connected()) {

   Serial.println("Client Connected");

   char c = client.read();
   Serial.println(c);
   
   if(c == 'w'){

    Serial.println("Starting Right Motor");

    start_r();
    }
    
    if(c == 't'){

    Serial.println("Starting Left Motor");

    start_l();
    
 }     

if(c == 's'){

    Serial.println("Both Stop");

    both_stop();
    
 }     
}
}
void start_r()
{

   //right motor forward
   
    digitalWrite(IN3 ,HIGH);
     digitalWrite(IN4 ,LOW);
}

void start_l()
{
  //left motor forward
  digitalWrite (IN1,HIGH);
  digitalWrite (IN2,LOW);
  
}

void both_stop()
{
  //left motor stop
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,LOW);

   //right motor stop
    digitalWrite(IN3,LOW);
    digitalWrite(IN4,LOW);
}
