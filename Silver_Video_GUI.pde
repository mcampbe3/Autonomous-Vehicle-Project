import controlP5.*;
import processing.net.*;
import meter.*;

ControlP5 cp5;
Client myClient;
String data;
Meter m;
Meter n;
int enable_signal = 0;
int distance = 0;
int swap = 0;

PFont p; 
PFont i;
PFont d;

void setup() {
  size(700, 900);
  
  p = createFont("Arial", 20, true); 
  i = createFont("Arial", 20, true); 
  d = createFont("Arial", 20, true); 
  
  frameRate(30);
  cp5 = new ControlP5(this);
  myClient = new Client(this,"192.168.4.1",80);
 
  cp5.addButton("Start")
    .setValue(0)
    .setPosition(100, 150)
    .setSize(150, 75); 

  cp5.addButton("Stop")
    .setValue(0)
    .setPosition(300, 150)
    .setSize(150, 75); 
    
  m = new Meter(this, 100, 300);
    m.setDisplayDigitalMeterValue(true);
    m.setTitle("Enable Signal (Volts)");
    m.setMinScaleValue(0);
    m.setMaxScaleValue(5);
    m.setMinInputSignal(0);
    m.setMaxInputSignal(255);
    
  n = new Meter(this, 100, 600);
    n.setDisplayDigitalMeterValue(true);
    n.setTitle("Distance behind Guide (cm)");
    n.setMinScaleValue(0);
    n.setMaxScaleValue(50);
    String[] scaleLabels = {"0", "10", "20", "30", "40", "50"};
    n.setScaleLabels(scaleLabels);
    n.setMinInputSignal(0);
    n.setMaxInputSignal(50);

}


void draw() {

  if (myClient.active()){  
    
    textFont(p, 20);                 
    fill(0);                          
    text("Kp: 12", 500, 150);
    
    textFont(i, 20);                 
    fill(0);                          
    text("Ki: 0.00001", 500, 175);
    
    textFont(d, 20);                 
    fill(0);                          
    text("Kd: 2", 500, 200);
    
    myClient.write("k");        
    if (myClient.available() > 0) {
      enable_signal = myClient.read();     
    }         
     
    myClient.write("d");  
    if (myClient.available() > 0) {
      distance = myClient.read();     
    }    
    
    if ((distance > 30) && (enable_signal > 0))  { 
      // if the GUI mistakes the distance int for the enable_signal
      // update the enable with the correct value
      swap = enable_signal;
      enable_signal = distance;
         
      distance = swap;
      
      if (distance < 0) {
        distance = 0;
      }
      
      if (distance > 50) {
        distance = 0;
      }
      
      m.updateMeter(enable_signal);
      n.updateMeter(distance);
      
      if ((distance < 10) && (enable_signal < 100)) {
        println("Too close! Buggy stopped." + "\n");
      }
      
      else {
        println("" + "\n");
      }
 
    }    
    
    else if ((distance <= 50) && (enable_signal > 0)) {
      // if the correct integers are assigned to each variable...
      if (distance < 0) {
        distance = 0;
      }
      
      if (distance > 50) {
        distance = 0;
      }
      
      // if the value held by distance is valid    
      m.updateMeter(enable_signal); 
      n.updateMeter(distance);  
      
      if ((distance < 10) && (enable_signal < 100)) {
        println("Too close! Buggy stopped." + "\n");
      }
      
      else {
        println("" + "\n");
      }
    }
  }
}

public void Start() {
  if (myClient.active()){
    myClient.write("t");
    println("'Start' button pressed.");
  }
}

public void Stop() {
  if  (myClient.active()){
    myClient.write("s");
    println("'Stop' button pressed.");  
  }  
}
