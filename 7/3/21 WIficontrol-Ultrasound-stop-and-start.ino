#include <WiFiNINA.h>

/////////////////////////////////////////////////////////////
//WiFi
char ssid[] = "hello2";

char pass[] = "matthewhs";

WiFiServer server(80);
//////////////////////////////////////////////////////////////
//Arduino Pins
//Ultrasound pins
const int US_TRIG = 14;// arduino pin number
const int US_ECHO = 15;// arduino pin number

//IR eyes
const int LEYE = 21;
const int REYE = 13;

//PWM output pins
const int en1= 17;
const int en2 = 16;

//Motor output pins
const int IN1 = 3;   // Left motor(sitting in it) positive (forward @ high)
const int IN2 = 2;  //Left motor(sitting in it) negative (reverse @ high)
const int IN3 = 9;  //Right motor(sitting in it) postive ( forward @ high)
const int IN4 = 10; //Right motor(sitting in it) negative (reverse @ high)
//////////////////////////////////////////////////////////////////////////////
//Variables
char user_input;
const int d= 11;
int obstacleDetect = 0;
//////////////////////////////////////////////////////////////////////////////

void setup() {

   pinMode(US_TRIG, OUTPUT);//send out signal to be reflected back
  pinMode(US_ECHO, INPUT);// reads in a signal to calculate its distance away

  pinMode( LEYE, INPUT );
  pinMode( REYE, INPUT );

  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  
 Serial.begin( 9600 );
 
 WiFi.beginAP (ssid, pass);

 IPAddress ip = WiFi.localIP();

 Serial.print("IP Address:");

 Serial.println(ip); 

 server.begin();
}

void loop() {
  
  WiFiClient client = server.available();

  int distance;
  long duration;

  digitalWrite( US_TRIG, LOW );
  delayMicroseconds(2);
  digitalWrite( US_TRIG, HIGH );
  delayMicroseconds( 10 );
  digitalWrite( US_TRIG, LOW );

  duration = pulseIn( US_ECHO, HIGH );

  distance = duration/58;

  Serial.print("Distance detected: ");
  Serial.print( distance );
  Serial.println(" cm");

if (distance > d){

   Serial.print("Distance detected: ");
  Serial.print( distance );
  Serial.println(" cm");
  //Serial.println("Client Connected");
     char c = client.read(); 
    if (c =='t'){
       
      Serial.println("forward");
      both_forward();
      
    }
 
    if (c=='s'){
      Serial.println("stop");
      both_stop();
    }
}
else {
   Serial.print("Distance detected: ");
  Serial.print( distance );
  Serial.println(" cm");
  both_stop();}
}



/////////////////////////////////////////////////////////////////////
void Start_Lap(){
   int distance;
  long duration;

  digitalWrite( US_TRIG, LOW );
  delayMicroseconds(2);
  digitalWrite( US_TRIG, HIGH );
  delayMicroseconds( 10 );
  digitalWrite( US_TRIG, LOW );

  duration = pulseIn( US_ECHO, HIGH );

  distance = duration/58;

   Serial.print("Distance detected: ");
  Serial.print( distance );
  Serial.println(" cm");
  //delay (750);

if (distance >= d ){

if (digitalRead(LEYE)==LOW){
 Serial.print("low - move forward");//low = no black line detected
   analogWrite(en1,200);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);
}
else { 
  Serial.print("high- black line detected STOP");
    analogWrite(en1,0);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);
  
}
  
//Right eye
   if (digitalRead(REYE)==LOW){
  
   //right motor forward
   analogWrite(en2,200);
   digitalWrite(IN3,HIGH);
   digitalWrite(IN4,LOW);
}
else{
    //stop
    Serial.print("high- black line detected STOP");
    
   analogWrite(en2,0);
   digitalWrite(IN3,HIGH);
   digitalWrite(IN4,LOW);
}
}

else {
  
    //left motor stop
  analogWrite(en1,0);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

   //right motor stop
   analogWrite(en2,0);
    digitalWrite(IN3,HIGH);
     digitalWrite(IN4,LOW);
}
}

////////////////////////////////////////////////////////////////////////
void follow_track()
{
  if (digitalRead(LEYE)==LOW){
 Serial.print("low - move forward");//low = no black line detected
   analogWrite(en1,200);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);
}
else { 
  Serial.print("high- black line detected STOP");
    analogWrite(en1,0);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);
  
}
  
//Right eye
   if (digitalRead(REYE)==LOW){
  
   //right motor forward
   analogWrite(en2,200);
   digitalWrite(IN3,HIGH);
   digitalWrite(IN4,LOW);
}
else{
    //stop
    Serial.print("high- black line detected STOP");
    
   analogWrite(en2,0);
   digitalWrite(IN3,HIGH);
   digitalWrite(IN4,LOW);
}
}
/////////////////////////////////////////////////////////////////////////////////////
void ultrasound(){
  int distance;
  long duration;

  digitalWrite( US_TRIG, LOW );
  delayMicroseconds(2);
  digitalWrite( US_TRIG, HIGH );
  delayMicroseconds( 10 );
  digitalWrite( US_TRIG, LOW );

  duration = pulseIn( US_ECHO, HIGH );

  distance = duration/58;

  Serial.print("Distance detected: ");
  Serial.print( distance );
  Serial.println(" cm");

  if (distance > d)
  {
  obstacleDetect=2;
  }
  
  if (distance <=d)
{
  obstacleDetect=1;
} }

/////////////////////////////////////////////////////////////////////////

void both_stop()
{
  //left motor stop
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,LOW);

   //right motor stop
    digitalWrite(IN3,LOW);
    digitalWrite(IN4,LOW);
}

////////////////////////////////////////////////////////////////////

void both_forward ()
{
  //left motor forward
  analogWrite(en1,255);
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);

   //right motor forward
   analogWrite(en2,255);
    digitalWrite(IN3,HIGH);
     digitalWrite(IN4,LOW);
}

//////////////////////////////////////////////////////////////////
